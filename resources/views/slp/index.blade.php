<!DOCTYPE html>
<html>
  <head>
    <title>SLP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <div class="logo">
	                 <h1><a style="font-family: 'Righteous', cursive;" href="index.html">Surat Lamaran Pekerjaan</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li class="current"><a> Informasi / Petunjuk</a></li>
                    <li><a>1. Isi informasi surat pada Form di samping</a></li>
                    <li><a>2. Lalu Klik Tombol 'Buat Surat Ini'</a></li>
                    <li><a>3. Surat berhasil dibuat dan akan diunduh otomatis</a></li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-7">
			<div class="row">
				<div class="col-md-12">
				<h2 style="font-family: 'Righteous', cursive; margin-bottom: 20px;">Format Surat Lamaran Pekerjaan (SLP)</h2>
				<hr style="color: black;">
				</div>
				<div class="col-md-12 panel-info">
					<div class="content-box-header panel-heading" style="background-color: blue; border-color: blue;">
						<div class="panel-title " style="color: white;">Template Surat Lamaran Pekerjaan</div>
					  
					  <div class="panel-options">
						  <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
						  <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					  </div>
					</div>
					<div class="content-box-large box-with-header">
					<div>
					<form action="{{ route('slp.print') }}" method="GET" class="form-horizontal" role="form" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="input1" class="col-sm-2 control-label">Penerima Surat</label>
							<div class="col-sm-10">
							<input type="text" class="form-control" id="input1" placeholder="contoh : HRD PT TerminalHRD" name="letter_receiver" required>
							</div>
						</div>
						<div class="form-group">
							<label for="input2" class="col-sm-2 control-label">Salam Pembuka</label>
							<div class="col-sm-10">
							<textarea class="form-control" id="input2" rows="3" name="greetings">Dengan Hormat,</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Paragraf 1</label>
							<div class="col-sm-10">
							<textarea class="form-control" placeholder="Textarea" rows="3" name="paragraf_1">Sehubungan dengan informasi yang saya peroleh bahwa di perusahaan Bapak/ Ibu sedang membuka lowongan pekerjaan, maka untuk itu saya yang bertanda tangan di bawah ini :</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Paragraf 2</label>
							<div class="col-sm-10">
							  <div class="form-group row">
							   <label class="col-sm-2 control-label">Nama</label>
							    <div class="col-sm-10">
							      <input type="text" class="form-control" id="" placeholder="Nama" name="name" required>
							    </div>
							  </div>
							  <div class="form-group row">
								<label class="col-sm-2 control-label">Tempat/Tgl.Lahir</label>
								 <div class="col-sm-10">
								   <input type="text" class="form-control" id="" placeholder="Tempat/Tgl.Lahir" name="date_of_birth" required>
								 </div>
							   </div>
							   <div class="form-group row">
								<label class="col-sm-2 control-label">Jenis Kelamin</label>
								 <div class="col-sm-10">
								   <input type="text" class="form-control" id="" placeholder="Laki - Laki / Perempuan" name="gender" required>
								 </div>
							   </div>
							   <div class="form-group row">
								<label class="col-sm-2 control-label">Agama</label>
								 <div class="col-sm-10">
								   <input type="text" class="form-control" id="" placeholder="Agama" name="religion" required>
								 </div>
							   </div>
							   <div class="form-group row">
								<label class="col-sm-2 control-label">Pendidikan Terakhir</label>
								 <div class="col-sm-10">
								   <input type="text" class="form-control" id="" placeholder="Pendidikan Terakhir" name="last-education" required>
								 </div>
							   </div>
							   <div class="form-group row">
								<label class="col-sm-2 control-label">Alamat</label>
								 <div class="col-sm-10">
								   <textarea class="form-control" id="" placeholder="Alamat" name="address" required></textarea>
								 </div>
							   </div>
							   <div class="form-group row">
								<label class="col-sm-2 control-label">Telepon (HP)</label>
								 <div class="col-sm-10">
								   <input type="text" class="form-control" id="" placeholder="Telepon (HP)" name="phone_number" required>
								 </div>
							   </div>
							</div>
						</div>
						<div class="form-group">
							<label for="input3" class="col-sm-2 control-label">Paragraf 3</label>
							<div class="col-sm-10">
							<textarea class="form-control" id="input3" rows="3" name="paragraf_3">Dengan ini saya mengajukan permohonan kepada Bapak/ Ibu, kiranya dapat menerima saya sebagai karyawan pada posisi .............. di perusahaan Bapak/ Ibu</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="input4" class="col-sm-2 control-label">Paragraf 4</label>
							<div class="col-sm-10">
							<textarea class="form-control" id="input4" rows="3" name="paragraf_4">Sebagai bahan pertimbangan Bapak/ Ibu bersama ini saya lampirkan :</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Paragraf <span id="p">5</span></label>	
							<div class="col-sm-9">
								<input type="text" class="form-control" value="Foto Copy Ijazah Terakhir" name="isi_surat[]">
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>
							<div class="col-sm-9 mb-3">
								<input type="text" class="form-control" value="Foto Copy KTP" name="isi_surat[]">
							</div>
						</div>
						<div id="list">
							<div class="form-group">
							  <label class="col-sm-2 control-label"></label>
							    <div class="col-sm-9 mb-3">
							    	  <input type="text" class="form-control" value="Pas Foto 3x4" name="isi_surat[]">
								</div>
							 </div>
						</div>
						<div class="form-group">
							<label style="margin-left: 20px;" class="col-sm-2 control-label" >  </label>	
							<span class="text-primary card-link ml-3" id="add" style="cursor: pointer;">Tambah</span> 
							<span style="margin-left:20px ;" class="text-primary card-link mr-3" id="delete" style="cursor: pointer;">Hapus</span>
						</div>
						<div class="form-group">
							<label for="input5" class="col-sm-2 control-label">Salam Penutup</label>
							<div class="col-sm-10">
							<textarea class="form-control" id="input5" rows="3" name="closing">Demikian Surat Lamaran Kerja ini saya buat dengan sebenar-benarnya, besar harapan saya untuk dapat diterima di perusahaan Bapak/ Ibu.</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tempat Surat</label>	
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="contoh : Bogor" name="mailbox" required>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Nama Pengirim</label>	
							<div class="col-sm-9">
								<input type="text" class="form-control" placeholder="contoh : Obet Ajah" name="sender" required>
							</div> 
						</div>
						<hr>
						<div class="form-group">
							<div class="col-sm-9">
								<button class="btn btn-primary">Buat Surat Ini</button>
							</div> 
						</div>
					  </form>
					 </div>
				  </div>
				</div>
			</div>
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2020 <a href='#'>Website Template Surat Lamaran Pekerjaan</a>
            </div>
            
         </div>
      </footer>

    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/custom.js') }}"></script>
	<script>
		$('#add').on('click', function(){

		$('#list').append(`
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-9 mb-3">
								<input type="text" class="form-control" placeholder="Ketikkan Sesuatu" name="isi_surat[]" required>
						</div>
					</div>
			`);
		});

		$('#delete').on('click', function(){
		$('#list div:last').remove();
		});

		$('#delete').on('click', function(){
		$('#list div:last').remove();
		});

	</script>
  </body>
</html>