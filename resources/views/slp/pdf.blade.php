<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        
    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 8px;
        margin-bottom: 20%;
        width: 100%;
    }

    .container {
        width: 600px;
        margin-left: 5%;
        margin-right: 20%;
        margin-top: 6%;
    }

    .text {
        line-height: 25px;
        font-weight: lighter;
        font-size: 13px ;
        font-family: 'Times New Roman';
    }

    .text-information {
        text-align: right;
        /* margin-left: 90%; */
        width: 100%;
        margin-bottom: 3%;
    }

    .box-information {
        width: 100%;
        margin-bottom: 3%;
    }

    .text-information-1 {
        margin-top: 0;
    }

    td {
        font-family: 'Times New Roman';
        font-weight: lighter;
        font-size: 13px;
        padding-left: 5%;
        width: -10%;
    }

    .table {
        width: 50%;
        font-size: 20px;
        line-height: 25px;
    }

    .td-1 {
        padding-left: 10%;
    }

    .footer {
        margin-top: 5%;
        margin-bottom: 20%;
    }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1 class="text-information text">{{ @$input['mailbox'] }}, 27 Oktober 2020</h1>
            <div class="box-information">
                <h1 class="text">kepada YTH</h1>
                <h1 class="text-information-1 text">{{ @$input['letter_receiver'] }}</h1>
                <h1 class="text-information-1 text">Di tempat,</h1>
            </div>
        </div>


        <div class="body-surat">
            <h1 class="text">{{ @$input['greetings'] }}</h1>
            <div class="box-paragraf-surat">
                <h1 class="text">{{ @$input['paragraf_1'] }}</h1>
            </div>

            <div class="box-keterangan-data-diri">
                <table class="table">

                    <tr>
                        <td >Nama</td>
                        <td class="td-1">: {{ @$input['name'] }}</td>
                    </tr>
                    <tr>
                        <td>Tempat/Tgl. Lahir </td>
                        <td class="td-1">: {{ @$input['date_of_birth'] }}</td>
                    </tr>
                    <tr>
                        <td>Jenis kelamin </td>
                        <td class="td-1">: {{ @$input['gender'] }}</td>
                    </tr>
                    <tr>
                        <td>Agama </td>
                        <td class="td-1">: {{ @$input['religion'] }}</td>
                    </tr>
                    <tr>
                        <td>Pendidikan terakhir </td>
                        <td class="td-1">: {{ @$input['last-education'] }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td class="td-1">: {{ @$input['address'] }}</td>
                    </tr>
                    <tr>
                        <td>Telepon (HP) </td>
                        <td class="td-1">: {{ @$input['phone_number'] }}</td>
                    </tr>
                </table>
            </div>


            <div class="box-paragraf-surat">
                <h1 class="text">{{ @$input['paragraf_3'] }}</h1>

                <h1 class="text">{{ @$input['paragraf_4'] }}</h1>

                <table class="table">

                    @foreach ($datas as $data)

                    <tr>
                        <td>{{ @$no++ }}. {{ @$data }}</td>
                    </tr>

                    @endforeach

                </table>

                <h1 class="text">Demikian surat lamaran kerja ini saya buat dengan sebenar-benarnya, besar harapan saya untuk dapat diterima di perusahaan Bapak/Ibu.</h1>

                <div class="footer">
                <h1 class="text-information text">Hormat saya</h1>
             </div>

             <h1 class="text-information text">{{ @$input['sender'] }}</h1>
            </div>  
        </div>
    </div>
</body>
</html>