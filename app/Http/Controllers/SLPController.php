<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PDF;

class SLPController extends Controller
{
    public function index(Request $req)
    {
        return view('slp.index');
    }

    public function print(Request $req)
    {
        $input = $req->all();

        $datas = $input['isi_surat'];

        $no = 1;

        $pdf = PDF::loadView('slp.pdf',compact('input','datas','no'));

        return $pdf->download('Cetak Surat Lamaran Pekerjaan (SLP).pdf');        
    }
}
